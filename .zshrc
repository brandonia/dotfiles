
solus="sudo eopkg upgrade"
ubuntu="sudo apt update && sudo apt upgrade"

   ##--------##
 #### Prompt ####
   ##--------##

#Segments

PS1="%F{141}%n%f@%F{141}%m%f %~ - %# "
export PS1

RPS1="<[%!]>"
export RPS1

   ##---------##
 #### Aliases ####
   ##---------##

alias l='ls -al --color'
alias susp='systemctl suspend'
alias reboot='systemctl reboot'
alias grep='grep --color'
alias upgrade=$solus
alias poweroff='systemctl poweroff'

neofetch
