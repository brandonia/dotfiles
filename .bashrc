##
##   _                         _             _
##  | |__  _ __ __ _ _ __   __| | ___  _ __ (_) __ _
##  | '_ \| '__/ _` | '_ \ / _` |/ _ \| '_ \| |/ _` |
##  | |_) | | | (_| | | | | (_| | (_) | | | | | (_| |
##  |_.__/|_|  \__,_|_| |_|\__,_|\___/|_| |_|_|\__,_|
##
##

#
# ~/.bashrc
#


export PS1="[\[\033[01;34m\]\u\[$(tput sgr0)\]@\[\033[01;33m\]\h \[\033[36m\]\W\[$(tput sgr0)\]]\$"
export RPS1="<[%!]>"

PATH="$PATH:/home/brandonia/.local/bin:/home/brandonia/Projects/scripts";export PATH



## Alias. 
## One day move to seperate files. Lazy
alias susp='systemctl suspend'
alias tmux='tmux -2'
alias ls='ls --color=auto'
alias grep='grep --color'
alias ll='ls -ll'
alias la='ls -al'

neofetch
