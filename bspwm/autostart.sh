#!/bin/bash

## BSPWM Autostart Script

sxhkd &
$HOME/.config/polybar/launch.sh &
feh --bg-scale $HOME/.config/bg.jpg &
dunst &
compton &
